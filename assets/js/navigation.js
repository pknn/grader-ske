$(document).ready(function () {
  $(window).scroll(function () {
    var windowScrollOffset = $(window)[0].pageYOffset
    var mainNav = $('.main.menu')
    var placeholderNav = $('.placeholder.menu')
    if (windowScrollOffset >= mainNav[0].offsetHeight) {
      mainNav.addClass('fixed')
      placeholderNav.addClass('placed')
    } else {
      mainNav.removeClass('fixed')
      placeholderNav.removeClass('placed')
    }
  })
})

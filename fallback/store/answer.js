import axios from "axios";

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  userList: [],
});

export const mutations = {
  setUserList(state, data) {
    state.userList = data;
  }
};

export const actions = {
  getFromUser({
    state,
    commit
  }, data) {
    let url = process.env.apiUrl + "/answers/user/" + data;
    return new Promise((resolve, reject) => {
      axios
        .get(url)
        .then(response => {
          commit('setUserList', response.data)
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  saveAssets({}, data) {
    let url = process.env.apiUrl + "/assets/generate";
    return new Promise((resolve, reject) => {
      axios
        .post(url, data)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  save({}, data) {
    let url = process.env.apiUrl + "/answers";
    return new Promise((resolve, reject) => {
      axios.post(url, data).then((response) => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    })
  },
  run({}, data) {
    let url = process.env.apiUrl + "/runners/run";
    return new Promise((resolve, reject) => {
      axios.post(url, data).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  saveOutput({}, data) {
    let url = process.env.apiUrl + '/answers/' + data._id;
    return new Promise((resolve, reject) => {
      axios.put(url, data).then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error);
      })
    })
  }
};

export const getters = {
  all: state => state.all,
  fromUser: state => state.userList,
};

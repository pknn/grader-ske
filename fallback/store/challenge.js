import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  all: [],
})

export const mutations = {
  setAll(state, data) {
    state.all = data
  }
}

export const actions = {
  get({
    commit
  }) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/challenges';
      return new Promise((resolve, reject) => {
        axios.get(url).then((response) => {
          commit('setAll', response.data);
          resolve(response);
        }).catch((error) => {
          console.error(error);
          reject();
        })
      })
    })
  },
  save({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      let method = data._id ? 'put' : 'post';
      let url = process.env.apiUrl + '/challenges/' + (data._id ? data._id : '');
      axios({
        method,
        url,
        data
      }).then((response) => {
        resolve(response);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  remove({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      let method = 'delete'
      let url = process.env.apiUrl + '/challenges/' + data[0];
      let headers = data[1];
      axios({
        method,
        url,
        headers
      }).then((response) => {
        resolve(response);
      }).catch((error) => {
        reject(error)
      })
    })
  },
}

export const getters = {
  all: state => state.all,
  info: state => id => state.all.find((value) => value._id === id),
  count: state => state.all.length,
  suggestion: state => state.all.slice(0, 1),
}

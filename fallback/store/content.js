import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  all: [],
})

export const mutations = {
  setAll(state, data) {
    state.all = data;
  },
}

export const actions = {
  get({
    commit
  }) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/contents';
      axios.get(url).then((response) => {
        commit('setAll', response.data);
        resolve(response.data);
      }).catch((error) => {
        console.error(error);
        reject();
      })
    })
  },
  save({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      let method = data._id ? 'put' : 'post';
      let url = process.env.apiUrl + '/contents/' + (data._id ? data._id : '');
      axios({
        method,
        url,
        data
      }).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        console.error(error);
        reject(error);
      })
    })
  },
  remove({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      let method = 'delete'
      let url = process.env.apiUrl + '/contents/' + data[0];
      let headers = data[1];
      axios({
        method,
        url,
        headers
      }).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        console.error(error);
        reject(error);
      })
    })
  }
}

export const getters = {
  all: state => state.all,
  info: state => id => state.all.find((value) => value._id === id),
  title: state => title => state.all.find((value) => value.title.toLowerCase() === title),
  count: state => state.all.length,
  suggestion: state => state.all.slice(0, 2)
}

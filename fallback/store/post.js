import axios from "axios";

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  all: [],
  userPost: []
});
export const mutations = {
  setAll(state, data) {
    state.all = data;
  },
  setUserPost(state, userId) {
    state.userPost = state.all.filter(post => {
      return post.author._id === userId;
    });
  }
};
export const actions = {
  get({
    commit
  }) {
    let url = process.env.apiUrl + "/posts";
    return new Promise((resolve, reject) => {
      axios
        .get(url)
        .then(response => {
          commit("setAll", response.data);
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  getFromUser({
    commit
  }, id) {
    commit('setUserPost', id);
  },
  save({
    commit
  }, data) {
    let method = data._id ? "put" : "post";
    let url = process.env.apiUrl + "/posts/" + (data._id ? data._id : "");
    return new Promise((resolve, reject) => {
      axios({
          method,
          url,
          data
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  saveComment({
    commit
  }, data) {
    let url = process.env.apiUrl + "/comments/";
    return new Promise((resolve, reject) => {
      axios.post(url, data).then((response) => {
        resolve(response);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  toggleComment({
    commit
  }, data) {
    let url = process.env.apiUrl + "/comments/" + data._id;
    return new Promise((resolve, reject) => {
      axios.put(url, data).then((response) => {
        resolve(response);
      }).catch((error) => {
        reject(error);
      })
    })
  }

};

export const getters = {
  all: state => state.all,
  fromUser: state => state.userPost,
  info: state => id => state.all.find(value => value._id === id),
};

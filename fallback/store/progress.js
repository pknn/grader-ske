const axios = require('axios');

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  progress: null,
});

export const mutations = {
  setProgress(state, data) {
    state.progress = data
  },
}

export const actions = {
  init({}, userId) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/progress/' + userId;
      axios.post(url).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  get({
    commit
  }, userId) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/progress/' + userId;
      axios.get(url).then((response) => {
        commit('setProgress', response.data);
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  increment({}, data) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/progress';
      axios.put(url, data).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  }
}

export const getters = {
  progress: state => state.progress
}

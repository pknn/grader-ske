import axios from "axios";

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  all: []
});

export const mutations = {
  setAll(state, data) {
    state.all = data;
  }
};

export const actions = {
  save({
    commit
  }, data) {
    let method = data._id ? "put" : "post";
    let url = process.env.apiUrl + "/sections/" + (data._id ? data._id : "");
    return new Promise((resolve, reject) => {
      axios({
          method,
          url,
          data
        })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  get({
    commit
  }) {
    let url = process.env.apiUrl + "/sections";
    return new Promise((resolve, reject) => {
      axios
        .get(url)
        .then(response => {
          commit("setAll", response.data);
          resolve(response.data);
        })
        .catch(error => {
          reject(response.data);
        });
    });
  },
  remove({
    commit
  }, data) {
    return new Promise((resolve, reject) => {
      let method = "delete";
      let url = process.env.apiUrl + "/sections/" + data[0];
      let headers = data[1];
      axios({
          method,
          url,
          headers
        })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          console.error(error);
        });
    });
  }
};

export const getters = {
  all: state => state.all,
  info: state => id => state.all.find(value => value._id === id),
  title: state => title => state.all.find(value => value.title.toLowerCase() === title),
  count: state => state.all.length
};

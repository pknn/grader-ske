import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.post['Accept'] = 'application/json';

axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*'


export const state = () => ({
  info: null,
  // token: null,
  fallback: null,
});

export const mutations = {
  setUser(state, data) {
    state.info = data;
  },
  // setToken(state, token) {
  //   state.token = token;
  // },
};

export const actions = {
  get: ({
    state,
    commit
  }) => {
    let method = 'get';
    let url = process.env.apiUrl + '/users';
    let headers = {
      'x-auth': state.token,
      'username': state.info.username,
    };
    return new Promise((resolve, reject) => {
      axios({
        method,
        url,
        headers
      }).then((response) => {
        commit('setUser', response.data);
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  save: ({
    state,
  }, data) => {
    let method = 'post';
    let url = process.env.apiUrl + '/users/admin';
    return new Promise((resolve, reject) => {
      axios({
        method,
        url,
        data,
      }).then((response) => {
        resolve(response);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  login: ({
    state,
    commit
  }, data) => {
    let url = process.env.apiUrl + '/auth/login';
    return new Promise((resolve, reject) => {
      axios.post(url, data).then((response) => {
        state.fallback = response.data;
        commit('setUser', response.data);
        // commit('setToken', response.headers['x-auth']);
        resolve(response.data);
      }).catch((error) => {
        console.error(error);
        reject(error);
      })
    })
  },
  logout: ({
    state,
    commit
  }) => {
    // let headers = {
    //   'x-auth': state.token,
    //   'username': state.info.username,
    // };
    let url = process.env.apiUrl + '/auth/logout';
    return new Promise((resolve, reject) => {
      axios({
        headers,
        method: 'delete',
        url
      }).then((response) => {
        commit('setUser', null);
        commit('setToken', null);
        resolve(response.status);
      }).catch((error) => {
        reject(error);
      })
    })
  }
};

export const getters = {
  getUser: state => state.info,
  // getToken: state => state.token,
  fullName: state => {
    return state.info.firstName + ' ' + state.info.lastName;
  },
  fallback: state => state.fallback,
  bioLength: state => state.info.bio.length,
  auth: state => {
    return {
      'x-auth': state.token,
      'username': state.info.username
    }
  }
};

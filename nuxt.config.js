const webpack = require('webpack')

module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'SosCharger',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Grader for SKE16'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }, ],
  },
  /*
   ** Load css files
   */
  css: [
    '@/assets/vendor/semantic/semantic.min.css',
    '@/assets/vendor/animate.css',
    '@/assets/vendor/box-shadows.min.css',
    '@/assets/scss/_base.scss',
    'codemirror/lib/codemirror.css',
    'codemirror/addon/merge/merge.css',
    'codemirror/theme/base16-dark.css'
  ],

  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#fda085'
  },

  plugins: [{
      src: '~/assets/vendor/semantic/semantic.min.js',
      ssr: false,
    },
    {
      src: '~/assets/js/navigation.js',
      ssr: false,
    },
    {
      src: '~plugins/nuxt-codemirror-plugin.js',
      ssr: false
    }
  ],
  /*
   ** Build configuration
   */
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        '$': 'jquery',
        'jQuery': 'jquery',
        'jquery': 'jquery',
        'window.jQuery': 'jquery',
        '_': 'lodash',
        // ...etc.
      }),
    ],
    vendor: ['axios'],
    /*
     ** Run ESLint on save-
     */
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  head: {
    title: 'May the peace be with you',
    titleTemplate: 'DevInPeace | %s',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Meta description'
      }
    ]
  },
  router: {
    mode: 'hash'
  },
  transition: {
    name: 'page',
    mode: 'out-in'
  },
  env: {
    apiUrl: process.env.NODE_ENV === 'production' ? 'https://api.ske.space/api' : 'http://localhost:3001/api'
  }
}

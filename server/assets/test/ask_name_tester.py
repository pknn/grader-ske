from grader import *


@test_cases(
    ["Wisa", "Pknn"],
    description="Print My name is ???"
)
def askName(m, name):
    correct = False

    name = m.stdin.put(name)

    output = (m.stdout.read().rstrip())

    if output == "My name is {name}":
        correct = True

    assert correct, ('Expected My name is {name}. Got {output}')

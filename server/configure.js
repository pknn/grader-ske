const routes = require('./routes'),
  bodyParser = require('body-parser'),
  cors = require('cors');

module.exports = function (app) {

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));

  // const corsConfig = {
  //   "origin": "*",
  //   // "exposedHeaders": ['x-auth', 'username']
  // }
  // app.use(cors(corsConfig));
  routes.initialize(app);


  return app;
};

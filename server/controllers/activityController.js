const _ = require("lodash");

const Activity = require("./../models/Activity");
const User = require("./../models/User");

module.exports = {
  createActivity: function (req, res) {
    const body = _.pick(req.body, "description", "path", "postTitle");
    const id = _.pick(req.body, "userId");

    const activity = new Activity(body);
    User.findById(id.userId)
      .then(user => {
        activity.user = user;
        activity
          .save()
          .then(response => {
            res.status(201).send();
          })
          .catch(err => {
            res.status(400).send();
          });
      })
      .catch(err => {
        console.log(err)
        res.status(400).send();
      });
  },

  getUserActivity: function (req, res) {
    const userId = req.params.userId;
    User.findById(userId)
      .then(user => {
        Activity.find()
          .populate("user")
          .then(activities => {
            const userActivities = activities.filter(
              activity => activity.user._id === user._id
            );
            res.send(userActivities);
          })
          .catch(err => {
            res.status(400).send();
          });
      })
      .catch(err => {
        res.status(400).send();
      });
  },
  getAll: function (req, res) {
    Activity.getAll()
      .then(activities => {
        res.send(activities);
      })
      .catch(error => {
        res.status(400).send();
      });
  }
};

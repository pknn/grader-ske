const _ = require('lodash');

const Answer = require('./../models/Answer');
const User = require('./../models/User');
const Challenge = require('./../models/Challenge');

module.exports = {
  createAnswer: function (req, res) {
    const body = _.pick(req.body, 'bodyUrl');
    const ids = _.pick(req.body, ['challengeId', 'userId']);

    const answer = new Answer();
    const user = User.findById(ids.userId);
    const challenge = Challenge.findById(ids.challengeId);

    Promise.all([user, challenge]).then((response) => {
      answer.bodyUrl = body.bodyUrl;
      answer.user = response[0];
      answer.challenge = response[1];
      answer.save().then((response) => {
        res.status(201).send(response);
      })
    }).catch((error) => {
      res.status(400).send();
    })
  },

  updateAnswer: function (req, res) {
    const body = _.pick(req.body, ['outputs']);
    Answer.findById(req.params.answerId).then((answer) => {
      answer.outputs = body.outputs;
      answer.save().then((response) => {
        res.send(response);
      }).catch((error) => {
        res.status(400).send(error);
      })
    })
  },

  getAnswerOfUser: function (req, res) {
    Answer.find().then((answers) => {
      User.findById(req.params.userId).then((user) => {
        const userAnswer = answers.filter(value => {
          return value.user.toString() === user._id.toString()
        })
        res.send(userAnswer);
      }).catch(err => res.status(400).send(err))
    }).catch(err => res.status(400).send(err))
  },

  getAnswerOfChallenge: function (req, res) {
    Answer.find().then((answers) => {
      Challenge.findById(req.params.challengeId).then((challenge) => {
        const challengeAnswer = answers.filter(value => value.challenge.toString() === challenge._id.toString())
        res.send(challengeAnswer);
      }).catch(err => res.status(400).send(err))
    }).catch(err => res.status(400).send(err))
  },
}

const _ = require('lodash');
const fs = require('fs');
const {
  ObjectId
} = require('mongodb')

const User = require('./../models/User');
const Challenge = require('./../models/Challenge');

module.exports = {
  generateFile: function (req, res) {
    const data = req.body.code;
    const path = './server/assets/answer'
    const fileName = new ObjectId();
    const url = `${path}/${fileName}.py`

    fs.writeFile(url, data, (error) => {
      if (error) res.status(400).send(error);

      res.status(201).send(url);
    });
  },
}

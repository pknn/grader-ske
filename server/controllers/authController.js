const _ = require('lodash');

const User = require('../models/User');

module.exports = {
  login: function (req, res) {
    const body = _.pick(req.body, ['username', 'password']);

    User.verifyUserCredential(body.username, body.password)
      .then(user => user.generateAuthToken()
        .then((token) => {
          // res.header('Access-Control-Allow-Origin', '*').send(user);

          res.header('x-auth', token).send(user);
        })
      ).catch((error) => {
        console.log(error)
        res.status(400).send(error);
      });
  },

  logout: function (req, res) {
    const username = req.headers.username
    console.log(req.headers)
    User.getUserByUsername(username).then((user) => {
      user.removeAuthToken(req.token)
        .then((response) => {
          console.log(response)
          res.status(200).send();
        })
        .catch(() => {
          res.status(400).send();
        });
    })
  }
};

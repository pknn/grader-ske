const _ = require('lodash');

const Challenge = require('../models/Challenge');
const Section = require('../models/Section');

function insert(element, res) {
  element.save()
    .then(() => {
      res.status(201).send()
    })
    .catch((err) => {
      res.status(404).send(err)
    });
}

module.exports = {
  createChallenge: function (req, res) {
    const body = _.pick(req.body, ['title', 'description', 'body', 'sectionId']);
    const challenge = new Challenge(body);

    Section.findById(body.sectionId).then((section) => {
      challenge.section = section;
      section.challenges.push(challenge);
      Promise.all([challenge.save(), section.save()]).then((response) => {
        res.status(201).send();
      }).catch((error) => {
        console.log(error);
        res.status(400).send(error);
      })
    }).catch((error) => {
      console.log(error);
      res.status(400).send(error);
    })
  },

  getChallenge: function (req, res) {
    Challenge.getChallengeByTitle(req.header('title'))
      .then((challenge) => {
        res.status(200).send(challenge);
      })
      .catch((error) => {
        res.status(400).send(error);
      });
  },

  getAll: function (req, res) {
    Challenge.getAll().then((challenges) => {
      res.status(200).send(challenges);
    }).catch((error) => {
      res.status(400).send(error)
    })
  },

  updateChallenge: function (req, res) {
    const body = _.pick(req.body, ['_id', 'title', 'description', 'body', 'sectionId']);
    Challenge.findById(req.params.id).then((challenge) => {
      challenge.title = body.title || challenge.title;
      challenge.description = body.description || challenge.description;
      challenge.body = body.body || challenge.body;

      // Remove from old section
      Section.findById(challenge.section._id).then((oldSection) => {
        oldSection.challenges = oldSection.challenges.filter((value) => value.toString() != challenge._id.toString());
        oldSection.save().then(() => {
          // Add to new section
          Section.findById(body.sectionId).then((section) => {
            section.challenges.push(challenge);
            challenge.section = section;
            section.save().then(() => {
              insert(challenge, res);
            })
          })
        })
      }).catch((error) => {
        res.status(400).send();
      })
    }).catch((error) => {
      res.status(400).send();
    })
  },

  deleteChallenge: function (req, res) {
    // Remove challenge from database
    Challenge.findOneAndRemove(req.params.id).then((challenge) => {
      const actions = challenge.testCases.map((value) => {
        return TestCase.findByIdAndRemove(value);
      });
      // Remove testcase of challenge
      Promise.all(actions).then((response) => {
        // Remove challenge from section
        Section.findById(challenge.section).then((section) => {
          section.challenges = section.challenges.filter((value) => {
            return value !== req.params.id
          })
          console.log(section);
          section.save().then((response) => {
            res.status(200).send(response);
          })
        })
      }).catch((error) => {
        res.status(400).send();
      })
    }).catch((error) => {
      console.log(error);
      res.status(400).send();
    })
  }
};

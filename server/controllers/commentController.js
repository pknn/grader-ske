const _ = require('lodash');

const User = require('./../models/User');
const Post = require('./../models/Post');
const Comment = require('./../models/Comment');

module.exports = {
  createComment: function (req, res) {
    const body = _.pick(req.body, ['body']);
    const comment = new Comment(body);


    User.findById(req.body.authorId).then((user) => {
      comment.author = user;
      Post.findById(req.body.postId).then((post) => {
        post.comments.push(comment);
        post.save().then(() => {
          comment.isValidAnswer = false;
          comment.save().then((response) => {
            res.status(200).send();
          })
        })
      })
    }).catch((error) => {
      console.error(error)
      res.status(400).send();
    })
  },

  checkComment: function (req, res) {
    const valid = req.body.valid;
    const id = req.params.id;
    Comment.findById(id).then((comment) => {
      console.log(valid.toString())
      comment.isValidAnswer = !valid;
      comment.save().then((response) => {
        res.status(200).send();
      }).catch((error) => {
        console.log(error)
        res.status(400).send();
      })
    })
  },

  deleteComment: function (req, res) {
    const id = req.params.id;
    Comment.findByIdAndRemove(id).then((response) => {
      res.status(200).send();
    }).catch((error) => {
      res.status(400).send();
    })
  }
}

const _ = require('lodash');

const Section = require('./../models/Section');
const Content = require('../models/Content');

function insert(element, res) {
  element.save()
    .then(() => {
      res.status(201).send()
    })
    .catch((err) => {
      res.status(404).send(err)
    });
}

module.exports = {
  createContent: function (req, res) {
    const body = _.pick(req.body, ['title', 'description', 'body', 'sectionId']);
    const content = new Content(body);

    Section.findById(body.sectionId)
      .then((section) => {
        content.section = section;
        section.contents.push(content);
        section.save().then(() => {
          insert(content, res);
        })
      }).catch((err) => {
        console.log(err);
        res.status(400).send(err);
      });
  },

  updateContent: function (req, res) {
    const body = _.pick(req.body, ['_id', 'title', 'description', 'body', 'sectionId']);
    Content.getContentById(body._id).then((content) => {
      content.title = body.title || content.title;
      content.description = body.description || content.description;
      content.body = body.body || content.body;

      // Remove from old section
      Section.findById(content.section._id).then((oldSection) => {
        oldSection.contents = oldSection.contents.filter(value => value.toString() != content._id.toString());
        oldSection.save().then(() => {
          // Add to new section
          Section.findById(body.sectionId).then((section) => {
            section.contents.push(content);
            content.section = section;
            section.save().then(() => {
              insert(content, res);
            })
          })
        })
      })
    }).catch((error) => {
      console.log(error);
      res.status(400).send();
    });
  },

  getAll: function (req, res) {
    Content.getAll().then((contents) => {
      res.status(200).send(contents);
    }).catch((error) => {
      res.status(400).send();
    })
  },

  deleteContent: function (req, res) {
    // Find content and remove
    Content.findByIdAndRemove(req.params.id).then((content) => {
      // Remove content reference from section
      Section.findById(content.section).then((section) => {
        section.contents = section.contents.filter((value) => value.toString() != req.params.id.toString());
        section.save().then((response) => {
          res.status(200).send(response);
        })
      })
    }).catch((error) => {
      res.status(400).send(error);
    })
  }
};

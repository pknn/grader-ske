const _ = require('lodash');

const Post = require('./../models/Post');
const Comment = require('./../models/Comment');
const User = require('./../models/User');

module.exports = {
  createPost: function (req, res) {
    const body = _.pick(req.body, ['title', 'body']);
    const post = new Post(body);
    User.findById(req.body.authorId).then((user) => {
      post.author = user;
      post.save().then((response) => {
        res.status(201).send(response);
      })
    }).catch((error) => {
      res.status(400).send();
    })
  },

  updatePost: function (req, res) {
    const body = _.pick(req.body, ['title', 'body']);
    const post = new Post(body);
    post._id = req.params.id;
    Post.findByIdAndUpdate(post._id, post).then((response) => {
      res.status(200).send();
    }).catch((error) => {
      res.status(400).send();
    })
  },

  deletePost: function (req, res) {
    const id = req.params.id;
    Post.findByIdAndRemove(id).then((response) => {
      const actions = response.comments.map((value) => {
        return Comment.findByIdAndRemove(value);
      })
      Promise.all(actions).then((response) => {
        res.status(200).send();
      })
    }).catch((error) => {
      res.status(400).send();
    })
  },

  getAll: function (req, res) {
    Post.getAll().then((posts) => {
      res.status(200).send(posts);
    }).catch((error) => {
      console.log(error)
      res.status(400).send(error);
    })
  }
}

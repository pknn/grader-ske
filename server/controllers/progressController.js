const _ = require('lodash');

const User = require('./../models/User');
const Progress = require('./../models/Progress');

module.exports = {
  initial: function (req, res) {
    const userId = req.params.userId;
    User.findById(userId).then((user) => {
      const progress = new Progress();
      progress.user = user;
      progress.save().then((response) => {
        res.status(201).send();
      }).catch((error) => {
        console.log(error)
        res.status(400).send();
      })
    })
  },
  finish: function (req, res) {
    const userId = req.body._id;
    const activity = req.body.activity;

    Progress.getByUser(userId).then((progress) => {
      progress[activity] += 1;
      progress.save().then(() => {
        res.status(200).send();
      }).catch((err) => {
        res.status(400).send();
      })
    }).catch((error) => {
      res.status(400).send();
    })
  },

  getByUser: function (req, res) {
    const userId = req.params.userId;
    Progress.getByUser(userId).then((progress) => {
      res.status(200).send(progress);
    }).catch((error) => {
      res.status(400).send();
    })
  }
}

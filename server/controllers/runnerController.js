const _ = require('lodash');
const {
  exec
} = require('child_process');

const Challenge = require('./../models/Challenge');

module.exports = {
  runTest: function (req, res) {
    exec(`python3 -m grader ./server/assets/test/${req.params.fileName}_tester.py ./server/assets/answer/${req.params.fileName}_solution.py`, (err, stdout, stderr) => {
      if (err) {
        console.log(err);
        res.status(400).send();
        return;
      }

      res.send(stdout);
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
    });
  },

  run: function (req, res) {
    const body = req.body;
    const codeUrl = decodeURIComponent(body.bodyUrl);

    Challenge.findById(body.challengeId).then((response) => {
      const testerFile = response.title.toLowerCase().replace(' ', '_');
      const testerUrl = './server/assets/test/' + testerFile + '_tester.py';
      exec(`python3 -m grader ${testerUrl} ${codeUrl}`, (err, stdout, stderr) => {
        if (err) {
          console.log(err);
          res.status(400).send();
          return;
        }
        res.send(stdout);
      });
    })
  }
}

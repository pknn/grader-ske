const _ = require('lodash');

const Section = require('../models/Section');
const Content = require('../models/Content');
const Challenge = require('../models/Challenge');

function insert(element, res) {
  element.save()
    .then(() => {
      res.status(200).send();
    })
    .catch((err) => {
      res.status(400).send();
    });
};

module.exports = {
  createSection: function (req, res) {
    const body = _.pick(req.body, ['title', 'description']);
    const section = new Section(body);

    section.save().then(() => {
        res.status(201).send();
      })
      .catch((err) => {
        res.status(400).send(err);
      });
  },

  updateSection: function (req, res) {
    const body = _.pick(req.body, ['_id', 'title', 'description']);

    Section.findById(body._id)
      .then((section) => {
        const sectionToUpdate = section;
        sectionToUpdate.title = body.title || section.title;
        sectionToUpdate.description = body.description || section.description;
        insert(sectionToUpdate, res);
      });

  },

  getAllSection: function (req, res) {
    Section.getAll().then((sections) => {
      res.status(200).send(sections);
    }).catch(() => {
      res.status(400).send();
    })
  },

  getSection: function (req, res) {
    Section.getSectionByTitle(req.params.title)
      .then((section) => {
        res.status(200).send(section);
      })
      .catch(() => {
        res.status(400).send();
      });
  },

  deleteSection: function (req, res) {
    Section.findByIdAndRemove(req.params.id).then((response) => {
      const contentActions = response.contents.map((content) => {
        return Content.findByIdAndRemove(content);
      })

      const challengeActions = response.challenges.map((challenge) => {
        return Challenge.findByIdAndRemove(challenge);
      })

      Promise.all(contentActions).then(() => {
        Promise.all(challengeActions).then(() => {
          res.status(200).send();
        })
      })
    }).catch((error) => {
      console.error(error)
      res.status(400).send();
    })
  }
};

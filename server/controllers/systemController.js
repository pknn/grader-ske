const version = "[0.4.6]";

module.exports = {
  version: function(req, res) {
    res.status(200).send(`version ${version}`);
  }
};

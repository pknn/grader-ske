const _ = require('lodash');

const User = require('../models/User');

module.exports = {
  createUser: function (req, res) {
    const body = _.pick(req.body, ['username', 'firstName', 'lastName', 'email', 'password']);
    const user = new User(body);
    user.role = 'User';
    user.bio = '';

    user.save()
      .then((user) => {
        user.generateAuthToken()
      })
      .then((token) => {
        res.header('x-auth', token).status(201).send(user);
      }).catch((err) => {
        console.log(err)
        if (err.code === 11000) res.status(409).send(err);
      });
  },

  createSuperUser: function (req, res) {
    const body = _.pick(req.body, ['username', 'firstName', 'lastName', 'email', 'password']);
    const user = new User(body);
    user.role = 'Admin';
    user.bio = '';

    user.save()
      .then(() => user.generateAuthToken())
      .then((token) => {
        res.header('x-auth', token).status(201).send();
      }).catch((err) => {
        if (err.code === 11000) res.status(409).send(err);
      });
  },

  getUser: function (req, res) {
    User.getUserByUsername(req.header('username'))
      .then((user) => {
        res.status(200).send(user);
      }).catch((err) => {
        res.status(400).send(err);
      });
  },

  updateUser: function (req, res) {
    const body = _.pick(req.body, ['username', 'firstName', 'lastName', 'email', 'bio']);
    const {
      username
    } = req.params;

    User.getUserByUsername(username)
      .then((user) => {
        user.username = body.username || user.username;
        user.email = body.email || user.email;
        user.firstName = body.firstName || user.firstName;
        user.lastName = body.lastName || user.lastName;
        user.bio = body.bio || user.bio;

        user.save()
          .then(() => {
            res.status(200).send();
          }).catch(() => {
            res.status(400).send();
          });
      }).catch(() => {
        res.status(404).send();
      })
  },

  updatePassword: function (req, res) {
    const body = _.pick(req.body, ['username', 'email', 'password']);
    const {
      username
    } = req.params;

    User.getUserByUsername(username)
      .then((user) => {

        user.username = body.username || user.username;
        user.email = body.email || user.email;
        user.password = body.password || user.password;

        user.save()
          .then(() => {
            res.status(200).send();
          }).catch(() => {
            res.status(400).send();
          });
      }).catch(() => {
        res.status(404).send();
      })
  },

  deleteUser: function (req, res) {
    const {
      username
    } = req.params;

    User.getUserByUsername(name)
      .then((user) => {
        user.remove()
          .then(() => {
            res.status(200).send();
          }).catch(() => {
            res.status(500).send();
          });
      }).catch(() => {
        res.status(404).send();
      });
  }
};

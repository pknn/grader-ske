const User = require('./../models/User');

const authenticate = (req, res, next) => {
  const token = req.header('x-auth');
  const username = req.header('username') || req.params.username;
  User.verifyUserAuthToken(username, token).then((user) => {
    if (!user) {
      return Promise.reject(new Error(`Can't find user with ${username} username`));
    }

    req.user = user;
    req.token = token;
    return next();
  }).catch((error) => {
    res.status(401).send(new Error(`Unauthorized access: ${error}`));
  });
};

const authenticateAdmin = (req, res, next) => {
  const token = req.header('x-auth');
  const username = req.header('username') || req.params.username;
  User.verifyUserAuthToken(username, token).then((user) => {
    if (!user) {
      return Promise.reject(new Error(`Can't find user with ${username} username`));
    }

    if (user.role !== 'Admin') {
      res.status(401).send(new Error(`Unauthorized access`))
    }

    req.user = user;
    req.token = token;
    return next();
  }).catch((error) => {
    res.status(401).send(new Error(`Unauthorized access: ${error}`));
  });
}

module.exports = {
  authenticate,
  authenticateAdmin
};

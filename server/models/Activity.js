const mongoose = require('mongoose');

const {
  Schema,
} = mongoose;

const activitySchema = new Schema({
  description: {
    type: String,
  },
  postTitle: {
    type: String,
  },
  path: {
    type: String,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  dateTime: {
    type: Date,
    default: Date.now
  }
});

activitySchema.statics.getAll = function getAll() {
  return new Promise((resolve, reject) => {
    this.find().then((activities) => {
      resolve(activities);
    }).catch((error) => {
      reject(error);
    })
  })
}

const Activity = mongoose.model('Activity', activitySchema);

module.exports = Activity;

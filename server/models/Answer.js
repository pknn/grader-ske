const mongoose = require('mongoose');

const {
  Schema,
} = mongoose;

const answerSchema = new Schema({
  bodyUrl: {
    type: String,
    require: false,
  },
  challenge: {
    type: Schema.Types.ObjectId,
    ref: 'Challenge',
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  outputs: [{
    type: String,
    trim: true,
  }],
  dateTime: {
    type: Date,
    default: Date.now
  }
});

answerSchema.statics.getAnswerById = function getAnswerById(id) {
  return this.findById(id).populate('challenge', 'user').then((response) => {
    if (!answer)
      return Promise.reject(new Error(`Can't find answer with id ${id}`));
    return Promise.resolve(answer);
  })
};

const Answer = mongoose.model('Answer', answerSchema);

module.exports = Answer;

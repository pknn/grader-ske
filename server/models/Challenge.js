const mongoose = require('mongoose');

const {
  ObjectId,
} = require('mongodb');

const {
  Schema,
} = mongoose;

const challengeSchema = new Schema({
  title: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 120,
    required: true,
  },
  description: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 100,
    required: false,
  },
  body: {
    type: String,
    trim: true,
    minlength: 1,
    required: true,
  },
  section: {
    type: Schema.Types.ObjectId,
    ref: 'Section',
  },
});

challengeSchema.statics.getChallengeByTitle = function getChallengeByTitle(title) {
  return this.findOne({
    title,
  }).populate('section').then((challenge) => {
    if (!challenge) return Promise.reject(new Error(`Can't find challenge with name ${title}`));
    return Promise.resolve(challenge);
  });
};

challengeSchema.statics.getAll = function getAll() {
  return this.find().populate('section').then((challenges) => {
    return Promise.resolve(challenges);
  })
}

const Challenge = mongoose.model('Challenge', challengeSchema);

module.exports = Challenge;

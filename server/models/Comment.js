const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const commentSchema = new Schema({
  body: {
    type: String,
    maxlength: 250,
    require: false,
    trim: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  isValidAnswer: {
    type: Boolean,
    require: true,
  },
  dateTime: {
    type: Date,
    default: Date.now,
  }
});

commentSchema.statics.getCommentById = function getCommentById(id) {
  return this.findById(id).populate('author')
    .then((comment) => {
      if (!comment) return Promise.reject(new Error(`Can't find testcase with id ${id}`));
      return Promise.resolve(comment);
    });
};

commentSchema.statics.getAll = function getAll() {
  return this.find().populate('author').then((comments) => {
    return Promise.resolve(comments);
  })
}

const Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;

const mongoose = require('mongoose');

const {
  Schema,
} = mongoose;

const contentSchema = new Schema({
  title: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 50,
    required: true,
  },
  description: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 300,
    required: true,
  },
  body: {
    type: String,
    trim: true,
    minlength: 1,
    required: true,
  },
  section: {
    type: Schema.Types.ObjectId,
    ref: 'Section',
    required: true,
  },
});

contentSchema.statics.getAll = function getAll() {
  return this.find().populate('section').then((content) => {
    return Promise.resolve(content);
  });
}

contentSchema.statics.getContentById = function getContentById(_id) {
  return this.findOne({
    _id
  }).populate('section').then((content) => {
    if (!content) {
      return Promise.reject((new Error(`Can't find section with id ${_id}`)));
    }

    return Promise.resolve(content);
  });
};

contentSchema.statics.getContentByTitle = function getContentByTitle(title) {
  return this.findOne({
    title
  }).populate('section').then((content) => {
    if (!content) {
      return Promise.reject((new Error(`Can't find section with title ${title}`)));
    }

    return Promise.resolve(content);
  });
};


const Content = mongoose.model('Content', contentSchema);

module.exports = Content;

const mongoose = require('mongoose');

const {
  Schema
} = mongoose;

const postSchema = new Schema({
  title: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 120,
    required: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  body: {
    type: String,
    maxlength: 1000,
    require: false,
    trim: true,
  },
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment',
  }],
  dateTime: {
    type: Date,
    default: Date.now
  },
});

postSchema.statics.getPostByTitle = function getPostByTitle(title) {
  return this.findOne({
      title
    }).populate('author').populate('comments')
    .then((post) => {
      if (!post) return Promise.reject(new Error(`Can't find post with name ${title}`));
      return Promise.resolve(post);
    });
};

postSchema.statics.getPostById = function getPostById(id) {
  return this.findById(id).populate('author').populate('comments')
    .then((post) => {
      if (!post) return Promise.reject(new Error(`Can't find post with id ${id}`));
      return Promise.resolve(post);
    });
}

postSchema.statics.getAll = function getAll(id) {
  return this.find().populate('author').populate({
    path: 'comments',
    populate: {
      path: 'author'
    }
  }).then((posts) => {
    return Promise.resolve(posts);
  })
}

const Post = mongoose.model('Post', postSchema);

module.exports = Post;

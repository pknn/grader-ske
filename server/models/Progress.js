const mongoose = require('mongoose');

const {
  ObjectId,
} = require('mongodb');

const {
  Schema,
} = mongoose;

const progressSchema = new Schema({
  content: {
    type: Number,
    default: 0,
  },
  challenge: {
    type: Number,
    default: 0,
  },
  followUp: {
    type: Number,
    default: 0,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
});

progressSchema.statics.getAll = function getAll() {
  return this.find().populate('user').then((progresses) => {
    return Promise.resolve(progresses);
  })
}

progressSchema.statics.getByUser = function getByUser(userId) {
  return this.find().populate('user').then((progresses) => {
    const progress = progresses.find(value => {
      return value.user._id == userId
    })
    return Promise.resolve(progress);
  })
}

const Progress = mongoose.model('Progress', progressSchema);

module.exports = Progress;

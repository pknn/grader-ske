const mongoose = require('mongoose');

const {
  Schema,
} = mongoose;

const sectionSchema = new Schema({
  title: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 120,
    required: true,
  },
  description: {
    type: String,
    trim: true,
    minlength: 1,
    maxlength: 150,
    required: true,
  },
  contents: [{
    type: Schema.Types.ObjectId,
    ref: 'Content',
  }, ],
  challenges: [{
    type: Schema.Types.ObjectId,
    ref: 'Challenge',
  }, ],
});

sectionSchema.statics.getAll = function getAll() {
  return this.find({}).populate('tags').populate('contents').populate('challenges').then((sections) => {
    return Promise.resolve(sections);
  })
}

const Section = mongoose.model('Section', sectionSchema);

module.exports = Section;

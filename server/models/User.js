const mongoose = require('mongoose');
const validator = require('validator');
const _ = require('lodash');
const bcrypt = require('bcryptjs');
const {
  ObjectId,
} = require('mongodb');

const {
  Schema,
} = mongoose;

const userSchema = new Schema({
  userId: {
    type: String,
    default: ObjectId(),
    trim: true,
  },
  firstName: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    validate: {
      validator: validator.isAlpha,
    },
  },
  lastName: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    validate: {
      validator: validator.isAlpha,
    },
  },
  email: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isEmail,
    },
  },
  username: {
    type: String,
    required: true,
    trim: true,
    minlength: 1,
    unique: true,
    validate: {
      validator: validator.isAlphanumeric,
    },
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
  },
  tokens: [{
    access: {
      type: String,
      required: true,
    },
    token: {
      type: String,
      required: true,
    },
  }],
  role: {
    type: String,
    required: true,
  },
  bio: {
    type: String,
    required: false,
    maxlength: 150,
  }
});

userSchema.methods.toJSON = function secureObjectJSONData() {
  const userObject = this.toObject();
  return _.pick(userObject, ['_id', 'email', 'username', 'firstName', 'lastName', 'bio', 'role', 'token']);
};

userSchema.methods.generateAuthToken = function generateAuthToken() {
  const access = 'auth';
  const salt = bcrypt.genSaltSync(10);
  const token = bcrypt.hashSync(this.userId, salt);

  this.tokens = this.tokens.filter(value => value.access !== 'auth');

  this.tokens.push({
    access,
    token,
  });

  return this.save().then(() => token);
};

userSchema.methods.removeAuthToken = function removeAuthToken(token) {
  return this.update({
    $pull: {
      tokens: {
        token,
      },
    },
  });
};

userSchema.statics.verifyUserAuthToken = function verifyUserAuthToken(username, token) {
  return this.findOne({
    username,
  }).then((user) => {
    if (!user) {
      return Promise.reject(new Error(`Can't find user with ${username} username`));
    }

    const authTokenObject = user.tokens.find(value => value.access === 'auth');
    return token === authTokenObject.token ? user : undefined;
  });
};

userSchema.statics.verifyUserCredential = function verifyUserCredential(username, password) {
  return this.findOne({
    username,
  }).then((user) => {
    if (!user) {
      return Promise.reject(new Error(`Can't find user with ${username} username`));
    }

    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (error, result) => {
        if (result) {
          resolve(user);
        } else {
          reject(error);
        }
      });
    });
  });
};

userSchema.statics.getUserByUsername = function getUserByUsername(username) {
  return this.findOne({
    username,
  }).then((user) => {
    if (!user) {
      return Promise.reject(new Error(`Can't find user with ${username} username`));
    }

    return Promise.resolve(user);
  });
};

userSchema.pre('save', function generateSecurePassword(next) {
  if (this.isModified('password')) {
    bcrypt.genSalt(10, (e, salt) => {
      bcrypt.hash(this.password, salt, (err, hash) => {
        this.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

const User = mongoose.model('User', userSchema);

module.exports = User;

const {
  authenticate,
  authenticateAdmin
} = require('./middlewares/authenticate');

const cors = require('cors');

const system = require('./controllers/systemController');
const user = require('./controllers/userController');
const challenge = require('./controllers/challengeController');
const section = require('./controllers/sectionController');
const content = require('./controllers/contentController');
const post = require('./controllers/postController');
const comment = require('./controllers/commentController');
const auth = require('./controllers/authController');
const runner = require('./controllers/runnerController');
const assets = require('./controllers/assetsController');
const answer = require('./controllers/answerController');
const activity = require('./controllers/activityController');
const progress = require('./controllers/progressController');

module.exports.initialize = function (app) {
  app.use(cors())
  app.options('*', cors())
  //SYSTEM
  app.get('/api/system/version', system.version);

  //AUTH
  app.post('/api/auth/login', auth.login);
  app.delete('/api/auth/logout', auth.logout)

  //USER
  app.get('/api/users', user.getUser);
  app.post('/api/users', user.createUser);
  app.post('/api/users/admin', authenticateAdmin, user.createSuperUser);
  app.post('/api/users/admin', user.createSuperUser);
  app.delete('/api/users/:username', authenticateAdmin, user.deleteUser);

  //CHALLENGE
  app.get('/api/challenges', challenge.getAll);
  app.get('/api/challenges/:id', challenge.getChallenge);
  app.post('/api/challenges', challenge.createChallenge);
  app.put('/api/challenges/:id', challenge.updateChallenge);
  app.delete('/api/challenges/:id', challenge.deleteChallenge);

  //SECTION
  app.post('/api/sections', section.createSection);
  app.put('/api/sections/:id', section.updateSection);
  app.get('/api/sections', section.getAllSection);
  app.get('/api/sections/:title', section.getSection);
  app.delete('/api/sections/:id', section.deleteSection);

  //CONTENT
  app.get('/api/contents', content.getAll);
  app.post('/api/contents', content.createContent);
  app.put('/api/contents/:id', content.updateContent);
  app.delete('/api/contents/:id', content.deleteContent);

  //POST
  app.get('/api/posts', post.getAll);
  app.post('/api/posts', post.createPost);
  app.put('/api/posts/:id', post.updatePost);
  app.delete('/api/posts/:id', post.deletePost);

  //COMMENT
  app.post('/api/comments', comment.createComment);
  app.put('/api/comments/:id', comment.checkComment);
  app.delete('/api/comments/:id', comment.deleteComment);

  //RUNNER
  app.get('/api/runners/test', runner.runTest);
  app.post('/api/runners/run', runner.run);

  //ASSETS
  app.post('/api/assets/generate', assets.generateFile);

  //ANSWER
  app.get('/api/answers/user/:userId', answer.getAnswerOfUser);
  app.get('/api/answers/challenge/:challengeId', answer.getAnswerOfChallenge);
  app.post('/api/answers', answer.createAnswer);
  app.put('/api/answers/:answerId', answer.updateAnswer);

  //ACTIVITY
  app.post('/api/activities', activity.createActivity);
  app.get('/api/activities/:userId', activity.getUserActivity);
  app.get('/api/activities', activity.getAll);

  //PROGRESS
  app.post('/api/progress/:userId', progress.initial);
  app.put('/api/progress', progress.finish);
  app.get('/api/progress/:userId', progress.getByUser);
}

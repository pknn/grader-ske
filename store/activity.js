const axios = require('axios');

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['Accept'] = 'application/json';

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

export const state = () => ({
  activity: null,
  all: [],
})

export const mutations = {
  setActivity(state, data) {
    state.activity = data
  },
  setAll(state, data) {
    state.all = data
  }
}

export const actions = {
  get({
    commit
  }, userId) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/activities/' + userId;
      axios.get(url).then((response) => {
        commit('setActivity', response.data);
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  },
  getAll({
    commit
  }) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/activities/';
      axios.get(url).then((response) => {
        commit('setAll', response.data);
        resolve(response.data);
      }).catch((error) => {
        reject(error)
      })
    })
  },
  save({}, data) {
    return new Promise((resolve, reject) => {
      let url = process.env.apiUrl + '/activities';
      axios.post(url, data).then((response) => {
        resolve(response.data);
      }).catch((error) => {
        reject(error);
      })
    })
  },
}

export const getters = {
  activity: state => state.activity,
  all: state => state.all,
  recent: state => state.all.slice(-4)
}
